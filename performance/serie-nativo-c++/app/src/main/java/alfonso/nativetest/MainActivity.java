package alfonso.nativetest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView textNative;
    //static final double LN2 = 0.69314;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textNative = (TextView) findViewById(R.id.native_text);
        /*textJava = (TextView) findViewById(R.id.java_text);
        textRxJava = (TextView) findViewById(R.id.rxjava_text);*/
    }

    @Override protected void onStart() {
        super.onStart();
        calculateWithJNI();
    }

    public void calculateWithJNI() {
        double serie;
        long startTime = System.currentTimeMillis();
        serie = doubleFromJNI();
        long elapsedTime = System.currentTimeMillis() - startTime;

        textNative.setText("Result from C: "+ serie + " \nTime: "+elapsedTime);
    }

     /*public void calculateWithJava(View v){
        double serie=0;
        long startTime = System.currentTimeMillis();

        serie = getSerie();
        long elapsedTime = System.currentTimeMillis() - startTime;
        textJava.setText("Result from Java: "+ serie + "\nTime: "+elapsedTime);
    }
    public void calculateWithRxJava(View v){
        long startTime = System.currentTimeMillis();
        Observable.fromCallable(() -> getSerie())
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        d -> {
                            long elapsedTime = System.currentTimeMillis() - startTime;
                            textRxJava.setText("Result from RxJava: "+ d + "\nTime: "+elapsedTime);
                        },
                        Throwable::printStackTrace);
    }

    double getSerie(){
        double serie=0;
        for (int i=1; i<=5; i++){
            for(int k=1; k<=100000; k++){
                serie = serie + ( Math.log(k)/LN2 ) + (3*k/2*i) + Math.sqrt(k) + Math.pow(k, i-1);
            }
        }
        return serie;
    }*/

    public native double doubleFromJNI();

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }
}