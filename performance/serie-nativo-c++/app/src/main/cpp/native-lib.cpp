#include <jni.h>
#include <string>
#include <cmath>

extern "C"
JNIEXPORT double JNICALL Java_alfonso_nativetest_MainActivity_doubleFromJNI(
        JNIEnv* env,
        jobject /* this */) {

    double serie = 0;
    double LN2 = 0.69314;
    for (int i=1; i<=10; i++)
        for(int k=1; k<=1000000; k++)
            serie = serie + ( std::log(k)/LN2 ) + (3*k/2*i) + std::sqrt(k) + std::pow(k, i-1);
    return serie;
}