package lidi.info.unlp.edu.ar.batterytest;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    static final double LN2 = 0.69314;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.text);
        calculate();
    }

    public void calculate() {
        //new CalculateTask().execute();
        double serie=0;
        long startTime = System.currentTimeMillis();

        for (int i=1; i<=10; i++){
            for(int k=1; k<=1000000; k++){
                serie = serie + ( Math.log(k)/LN2 ) + (3*k/2*i) + Math.sqrt(k) + Math.pow(k, i-1);
            }
        }
        long elapsedTime = System.currentTimeMillis() - startTime;
        //elapsedTime = elapsedTime/1000; //para verlo en segundos

        textView.setText("Result: "+String.valueOf(serie)+" Time:"+elapsedTime);

    }

   /* private class CalculateTask extends AsyncTask<Void,Void,Long>{
        double serie=0;
        long elapsedTime;

        @Override
//        protected Long doInBackground(Void... params) {
//            long startTime = System.currentTimeMillis();
//
//            for(int i=1; i<=5; i++){
//                for(int k=1; k<=500000; k++){
//                    serie = serie + ( Math.log(k)/LN2 ) + (3*k/2*i) + Math.sqrt(k) + Math.pow(k, i-1);
                }
            }
            long elapsedTime = System.currentTimeMillis() - startTime;
            return (Long)elapsedTime;
        }


        protected void onPostExecute(long result) {
            textView.setText("Time: "+result+" \nResult: "+String.valueOf(serie));
        }

    }*/
}
