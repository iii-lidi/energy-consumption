-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

local widget = require( "widget" )

local t = display.newText( "Waiting for button event...", 0, 0, native.systemFont, 18 )
t.x, t.y = display.contentCenterX, 70

local function onSystemEvent( event )
	if event.type == "applicationStart" then
		initial = os.clock()

		serie = 0;

		for j=1,10 do 
			for k=1,1000000 do
				serie = serie + ( math.log(k) / math.log(2) ) + (3*k/2*j) + math.sqrt(k) + math.pow(k, j-1);
			end
		end

		final = os.clock()
		tiempo = final - initial;	
		
		t.text = serie .. " -> " .. tiempo
	end

end

Runtime:addEventListener( "system", onSystemEvent )