function ready() {
    var inicio = new Date().getTime();
    $.resultado.text = 'calculando'; 

    var serie = 0;
    for ( var j=1; j <= 10; j++ ){
        for ( var k=1; k <= 1000000; k++ ){
            serie = serie + ( Math.log(k)/Math.LN2 ) + (3*k/2*j) + Math.sqrt(k) + Math.pow(k, j-1);
        }
    }
    var fin = new Date().getTime();
    var tiempo = fin - inicio;

    $.resultado.text = tiempo + ' -> ' + serie; 
}
$.index.addEventListener('open', ready);
$.index.open();
