﻿using Android.App;
using Android.Widget;
using Android.OS;
using Java.Lang;
using Android.Net;

namespace App1
{
    [Activity(Label = "App1", MainLauncher = true)]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            
            SetContentView(Resource.Layout.Main);
            var videoView = FindViewById<VideoView>(Resource.Id.SampleVideoView);
            string path = "android.resource://" + Application.PackageName + "/" + Resource.Raw.vid;

            videoView.SetVideoURI(Uri.Parse(path));
            videoView.Start();
        }
    }
}