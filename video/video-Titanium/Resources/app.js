// this sets the background color of the master UIView (when there are no windows/tab groups on it)
Titanium.UI.setBackgroundColor('#000');

/*var vidWin = Titanium.UI.createWindow({
    title : 'Video View Demo',
    backgroundColor : '#fff'
});
*/
var videoPlayer = Titanium.Media.createVideoPlayer({
    top : 2,
    autoplay : true,
    backgroundColor : 'black',
    height : 'auto',
    width : 'auto',
    fullscreen : true,
    url : 'vid.mp4'
});
/*var videoPlayer = Titanium.Media.createVideoPlayer({
    autoplay : true,
    backgroundColor : 'black',
   	mediaControlStyle : Titanium.Media.VIDEO_CONTROL_DEFAULT,
    scalingMode : Titanium.Media.VIDEO_SCALING_ASPECT_FILL,
    fullscreen : true
});*/
videoPlayer.play();
